# Travel Hacks

## Travel Data Sources
There are tons of useful travel-related data sources, as well as visualization and analysis tools! Some are very high level, and others are extremely detailed. A little googling will go a long way to uncover some really cool stuff! Here’s just a tiny sample of what can be found online.

We hope this will help get you started.


## Basics
https://en.wikipedia.org/wiki/World_Tourism_rankings


## General
#### Google Maps API
https://developers.google.com/maps/

#### CartoDB
A super easy app to create interactive maps:
https://cartodb.com/

#### Yelp
Business and reviews API:
https://www.yelp.com/developers/documentation/v2/overview


## Canada
#### General Canadian Tourism Overviews

http://en.destinationcanada.com/research/statistics-figures/international-visitor-arrivals

http://en.destinationcanada.com/research/statistics-figures/tourism-performance

#### Statistics Canada’s Data Portal
There’s lots of data at StatsCan’s website if you dig around for it:
http://www.statcan.gc.ca/start-debut-eng.html

For example, travel and tourism:
http://www.statcan.gc.ca/pub/11-402-x/2012000/chap/tt-ut/tt-ut-eng.htm

### Montreal
#### Open Data Portal
Lots of variety here:
http://donnees.ville.montreal.qc.ca/dataset

#### Tourism Data
(See the “Database” link for an Excel file full of data):
http://www.tourisme-montreal.org/Montreal-Tourism/Toolkit

#### Crime Data
http://www.montrealgazette.com/news/montreal-crime-stats/index.html

http://www.cbc.ca/news/canada/montreal/montreal-police-open-data-1.3555343

#### Public Transit

##### STM
http://www.stm.info/en/about/developers

##### BIXI
https://montreal.bixi.com/en/open-data

### Vancouver
#### BC Tourism Data
http://www.bcstats.gov.bc.ca/StatisticsBySubject/BusinessIndustry/Tourism.aspx

### Toronto
#### Tourism Overview
http://www1.toronto.ca/wps/portal/contentonly?vgnextoid=580b6fe8341da310VgnVCM10000071d60f89RCRD&vgnextchannel=401132d0b6d1e310VgnVCM10000071d60f89RCRD

#### Summary from the Greater Toronto Hotel Association
http://www.gtha.com/Industrystats/TourismStats.aspx


## United States
#### Bureau of Transportation Statistics
http://www.rita.dot.gov/bts/

#### US National Travel and Tourism Office
http://tinet.ita.doc.gov/

#### Inbound Travel Statistics
http://tinet.ita.doc.gov/outreachpages/inbound.general_information.inbound_overview.asp

#### Outbound Travel Statistics
http://tinet.ita.doc.gov/outreachpages/outbound.general_information.outbound_overview.asp

### Boston
#### Open Data
https://data.cityofboston.gov/

### Chicago
#### Open Data
https://data.cityofchicago.org/

### Miami
#### Open Data
https://opendata.miamidade.gov/

### New York City
#### Travel and Tourism Summary
http://www.nycedc.com/economic-data/travel-and-tourism

#### General Open Data
https://nycopendata.socrata.com/

### Los Angeles
#### Neighbourhood Data
http://maps.latimes.com/neighborhoods/

#### Open Data Portal
https://data.lacity.org/

### San Diego
http://www.sandiego.org/industry-research.aspx


## Europe
#### Tourism
http://ec.europa.eu/growth/sectors/tourism/index_en.htm

#### Statistics
http://ec.europa.eu/eurostat/statistics-explained/index.php/Tourism_statistics

http://ec.europa.eu/eurostat/web/tourism/data/database


## International
Tourism - Number of Arrivals:
http://data.worldbank.org/indicator/ST.INT.ARVL
